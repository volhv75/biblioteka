package com.example.controller;

import com.example.domain.book;
import com.example.domain.place;
import com.example.domain.reader;
import com.example.repository.bookrepository;
import com.example.repository.placerepository;
import com.example.repository.readerrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Volhv on 22.08.2016.
 */
@RestController
public class readcontroller {
    @Autowired
    readerrepository readerRepository;
    @Autowired
    placerepository placeRepository;
    @Autowired
    bookrepository bookRepository;

    @RequestMapping(value = "/reader_all")
    public List<reader> reader_all(){
        return readerRepository.findAll();
    }
    @RequestMapping(value = "/place_all")
    public List<place> place_all(){
        return placeRepository.findAll();
    }
    @RequestMapping(value = "/book_all")
    public List<book> book_all(){
        return bookRepository.findAll();
    }
    @RequestMapping(value = "/book_publisher/{publisher}")
    public List<book> book_publisher(@PathVariable String publisher){
        return bookRepository.findByPublisher(publisher);  }
    @RequestMapping(value = "/book_autor/{autor}")
    public List<book> book_autor(@PathVariable String autor){
        return bookRepository.findByAutor(autor);  }
    @RequestMapping(value = "/book_name/{name}")
    public List<book> book_name(@PathVariable String name){
        return bookRepository.findByBookname(name);  }
}
