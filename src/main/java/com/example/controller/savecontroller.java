package com.example.controller;

import com.example.domain.book;
import com.example.domain.place;
import com.example.domain.reader;
import com.example.repository.bookrepository;
import com.example.repository.placerepository;
import com.example.repository.readerrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Volhv on 22.08.2016.
 */
@Controller
public class savecontroller {
    @Autowired
    readerrepository readerRepository;
    @Autowired
    placerepository placeRepository;
    @Autowired
    bookrepository bookRepository;

    @RequestMapping(value = "/read/{name}/{pass}")
    public void reader_save(@PathVariable String name, @PathVariable String pass, reader reader){

        reader.setName_reader(name);
        reader.setPassport(pass);

        readerRepository.saveAndFlush(reader);
    }


    @RequestMapping(value = "/place/{room}/{cupboard}/{rack}/{position}")
    public void place_save(@PathVariable String room, @PathVariable String cupboard,
                           @PathVariable String rack, @PathVariable long position, place place){

       place.setRoom(room);
        place.setCupboard(cupboard);
        place.setRack(rack);
        place.setPosition(position);

        placeRepository.saveAndFlush(place);
    }

    @RequestMapping(value = "/book/{autor}/{name_book}/{isbn}/{annotation}/{publisher}/{place}/{reader}")
    public void book_save(@PathVariable String autor, @PathVariable String name_book,
                           @PathVariable String isbn, @PathVariable String annotation, @PathVariable String publisher,
                           @PathVariable long place,@PathVariable long reader, book book){

        book.setAutor(autor);
        book.setBookname(name_book);
        book.setIsbn(isbn);
        book.setAnnotation(annotation);
        book.setPublisher(publisher);
        book.setPlace_id(place);
        book.setReader_id(reader);

        bookRepository.saveAndFlush(book);
    }
}

