package com.example.repository;

import com.example.domain.book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Volhv on 19.08.2016.
 */
public interface bookrepository extends JpaRepository <book, Long> {
    List<book> findByBookname(String BookName);
    List<book> findByAutor(String Autor);
    List<book> findByPublisher(String Publisher);


}
