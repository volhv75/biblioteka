package com.example.repository;

import com.example.domain.reader;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Volhv on 19.08.2016.
 */
public interface readerrepository extends JpaRepository <reader, Long> {
}
