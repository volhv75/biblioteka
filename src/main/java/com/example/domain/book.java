package com.example.domain;

import org.hibernate.validator.constraints.Length;


import javax.persistence.*;

/**
 * Created by Volhv on 18.08.2016.
 */


@Entity
@Table(name = "book")
public class book {

    @Id
    @GeneratedValue
    private Long id;

    @Length(min = 2)
    private String bookname;
    @Length(min = 2)
    private String publisher;
    @Length(min = 2)
    private String autor;
    @Length(min = 2)
    private String isbn;
    @Column(name="annotation")
    private String annotation;
    @Column(name="place_id")
    private Long place_id;
    @Column(name="reader_id")
    private Long reader_id;


    public Long getId() {
        return id;
    }
    public book setId(long id) {
        this.id = id;
        return this;
    }


    public String getBookname() {
        return bookname;
    }
    public book setBookname(String bookname) {
        this.bookname = bookname;
        return this;}


    public String getPublisher() {
        return publisher;
    }
    public book setPublisher(String publisher) {
        this.publisher = publisher;
        return this;
    }

    public String getAutor() {
        return autor;
    }
    public book setAutor(String autor) {
        this.autor = autor;
        return this;
    }

    public String getIsbn() {
        return isbn;
    }
    public book setIsbn(String isbn) {
        this.isbn = isbn;
        return this;
    }

    public String getAnnotation() {
        return annotation;
    }
    public book setAnnotation(String annotation) {
        this.annotation = annotation;
        return this;
    }

    public Long getPlace_id() {
        return place_id;
    }
    public book setPlace_id(long place_id) {
        this.place_id = place_id;
        return this;
    }

    public Long getReader_id() {
        return reader_id;
    }
    public book setReader_id(long reader_id) {
        this.reader_id = reader_id;
        return this;
    }
}
