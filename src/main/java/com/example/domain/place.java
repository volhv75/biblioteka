package com.example.domain;



import javax.persistence.*;


/**
 * Created by Volhv on 18.08.2016.
 */

@Entity
@Table(name = "place")
public class place {
    @Id
    @GeneratedValue
    @Column (name="id")
    private long id;

    public long getId() {
        return id;
    }

    public place setId(long id) {
        this.id = id;
        return this;
    }

    @Column(name="room_book")
    private String room_book;
    @Column(name="cupboard")
    private String cupboard;
    @Column(name="rack")
    private String rack;
    @Column(name = "position")
    private long position;


    public String getRoom_book() {
        return room_book;
    }

    public place setRoom(String room_book) {
        this.room_book = room_book;
        return this;
    }


    public String getCupboard() {
        return cupboard;
    }

    public place setCupboard(String cupboard) {
        this.cupboard = cupboard;
        return this;
    }

    public String getRack() {
        return rack;
    }

    public place setRack(String rack) {
        this.rack = rack;
        return this;
    }



    public long getPosition() {
        return position;
    }

    public place setPosition(long position) {
        this.position = position;
        return this;

    }


}
