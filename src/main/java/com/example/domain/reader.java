package com.example.domain;

/**
 * Created by Volhv on 18.08.2016.
 */




import org.hibernate.validator.constraints.Length;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "reader")

public class reader {
    @Id
    @GeneratedValue
    private Long id;

    @Length(min = 2)
    private String name_reader;

    @Length(min = 2)
    private String passport;

    public Long getId() {
        return id;
    }

    public reader setId(long id) {
        this.id = id;
        return this;
    }

    public String getName_reader() {
        return name_reader;
    }

    public reader setName_reader(String name_reader) {
        this.name_reader = name_reader;
        return this;
    }

    public String getPassport() {
        return passport;
    }

    public reader setPassport(String passport) {
        this.passport =passport;
        return this;
    }
}



